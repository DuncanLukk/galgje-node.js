///<reference path="typings/server.d.ts"/>
var http = require('http'), server = http.createServer(handler), poort = process.env.PORT | '8000';
// De server start op een bepaalde poort en laat dit weten via de console
server.listen(poort);
console.log("Server ingeschakeld op poort " + poort);
function handler(request, response) {
    response.writeHead(200, { "Content-Type": 'text/plain' });
    response.write("De server staat aan");
    response.end();
    console.log('Server staat aan');
}
//De iolistener wordt aangemaakt
var io = require('socket.io'), ioListener = io.listen(server, {});
//Een random woord wordt gekozen uit de array
var Random;
(function (Random) {
    var woorden = ['naaste', 'tukker', 'roeidol', 'voornaam', 'scheur', 'seinhuis', 'speeluur', 'horloge',
        'traineren', 'spreng', 'starten', 'sleutel', 'oogstfeest', 'toevoegen', 'cataloog', 'doodstil', 'trailer',
        'vlassig', 'onthand', 'graskaas', 'scheen', 'minuut', 'opereren', 'lekken', 'cacaoboon', 'bloemkelk', 'sjiiet',
        'volume', 'ballon', 'rijpaard', 'seniel', 'galappel', 'meevallen', 'meerkoet', 'torsen', 'engagement', 'avance',
        'zuivel', 'feesten', 'digressie', 'rijten', 'reviseren', 'meeprater', 'tumult', 'kopwerk', 'keramiek',
        'traiteur', 'appelflap', 'oorlam', 'deciel', 'overhoren', 'mesten', 'chintz', 'palmboom', 'trapper',
        'aanhaling', 'pagaaien', 'broedei', 'bagatel', 'sloerie', 'indiaans', 'trefzeker', 'teisteren', 'pronkboon',
        'doofpot', 'gravel', 'roomboter', 'pantry', 'klaslokaal', 'onoorbaar', 'vuurbol', 'vangnet', 'expireren',
        'offshore', 'bereden', 'afloop', 'register', 'hendel', 'nylons', 'trilling', 'enerveren', 'radius', 'adhesie',
        'rammen', 'clerus', 'wassen', 'bastaard', 'loeven', 'bakkie', 'glanzen', 'bijtijds', 'kronkelen', 'meneer',
        'penant', 'tonrond', 'luiaard', 'verdoofd', 'rabbijn', 'zaadlob', 'bolletje'];
    function getWoord() {
        return woorden[Math.floor(Math.random() * woorden.length)];
    }
    Random.getWoord = getWoord;
})(Random || (Random = {}));
var GameModule;
(function (GameModule) {
    // --- Private ---
    var games = [];
    var Game = (function () {
        function Game(host) {
            this._gameOver = false;
            this._winnaar = null;
            this.host = host;
            this.controller = new GameController(this);
            this.woord = Random.getWoord();
        }
        Game.prototype.gameOver = function (winnaar) {
            this._gameOver = true;
            this._winnaar = winnaar;
        };
        Game.prototype.getGameOver = function () {
            return this._gameOver;
        };
        Game.prototype.heeftGewonnen = function (speler) {
            return this._winnaar && this._winnaar.id === speler.id;
        };
        return Game;
    }());
    // --- Public ---
    var GameController = (function () {
        function GameController(game) {
            this.game = game;
        }
        //Spelers worden aan elkaar gekoppeld als tegenstanders
        GameController.prototype.setTegenstander = function (tegenstander) {
            var game = this.game;
            game.tegenstander = tegenstander;
            console.log(game.host.naam + " en " + game.tegenstander.naam + ' zijn nu tegenstanders');
        };
        GameController.prototype.startGame = function () {
            this.emit('StartGame', {
                speler1: this.game.host.id,
                speler2: this.game.tegenstander.id
            });
        };
        GameController.prototype.emit = function (event, data) {
            this.game.host.socket.emit(event, data);
            this.game.tegenstander.socket.emit(event, data);
        };
        GameController.prototype.getRaadString = function (geradenLetters) {
            var woord = this.game.woord, raadString = '';
            for (var i = 0; i < woord.length; i++)
                raadString += (geradenLetters.indexOf(woord[i]) >= 0 ? woord[i] : '_') + ' ';
            return raadString;
        };
        GameController.prototype.getGameOver = function () {
            return this.game.getGameOver();
        };
        GameController.prototype.heeftGewonnen = function (speler) {
            return this.game.heeftGewonnen(speler);
        };
        GameController.prototype.raadLetter = function (speler, letter) {
            var game = this.game;
            if (game.woord.indexOf(letter) === -1) {
                if (speler.pogingen <= 1)
                    game.gameOver(speler);
                else
                    speler.pogingen--;
            }
        };
        GameController.prototype.gameOver = function (winnaar) {
            this.game.gameOver(winnaar);
        };
        GameController.prototype.spelerStopt = function (speler) {
            var game = this.game;
            this.gameOver(speler.id === game.host.id ? game.tegenstander : game.host);
        };
        return GameController;
    }());
    GameModule.GameController = GameController;
    var Speler = (function () {
        function Speler(socket, naam) {
            this.pogingen = 10;
            this.id = socket.id;
            this.naam = naam;
            this.socket = socket;
        }
        return Speler;
    }());
    GameModule.Speler = Speler;
    //Een nieuwe game wordt aangemaakt
    function newGame(speler) {
        var game = new Game(speler);
        games.push(game);
        return game.controller;
    }
    GameModule.newGame = newGame;
    //Waneer een game beschikbaar is, zal een speler die game joinen
    function joinGame(host, tegenstander) {
        var gameCtrl = null;
        for (var i = 0; i < games.length; i++)
            if (games[i].host === host)
                gameCtrl = games[i].controller;
        gameCtrl.setTegenstander(tegenstander);
        return gameCtrl;
    }
    GameModule.joinGame = joinGame;
})(GameModule || (GameModule = {}));
var randomWoord = Random.getWoord;
var GameController = GameModule.GameController;
var Speler = GameModule.Speler;
var wachtend = null; // Wachtende speler
var spelernr = 1;
ioListener.on('connection', function (socket) {
    var speler = new Speler(socket, 'Speler ' + spelernr++);
    console.log('JOIN: ' + speler.naam);
    var game = null, geradenLetters = [], kanStarten = false;
    // Tegenstander vinden of wachten
    if (wachtend !== null) {
        console.log('Er wacht al iemand op ' + speler.naam);
        game = GameModule.joinGame(wachtend, speler);
        kanStarten = true;
        wachtend = null;
    }
    else {
        console.log(speler.naam + ' moet wachten');
        game = GameModule.newGame(speler);
        wachtend = speler;
    }
    // Begin game
    socket.emit('GameCreated', {
        streepjes: game.getRaadString([]),
        pogingen: speler.pogingen
    });
    if (kanStarten)
        game.startGame();
    // Speler raadt
    socket.on('RaadLetter', function (letter) {
        var alGeraden = false;
        if (geradenLetters.indexOf(letter) >= 0) {
            // Letter is al geraden
            alGeraden = true;
        }
        else {
            geradenLetters.push(letter);
            game.raadLetter(speler, letter);
        }
        var raadString = game.getRaadString(geradenLetters);
        if (raadString.indexOf('_') < 0) {
            // Alles geraden
            game.gameOver(speler);
        }
        game.emit('Respons', {
            id: speler.id,
            letter: letter,
            alGeraden: alGeraden,
            raadString: raadString,
            pogingen: speler.pogingen,
            gameOver: game.getGameOver(),
            gewonnen: game.heeftGewonnen(speler)
        });
    });
    socket.on('disconnect', function () {
        // TODO: Disconnect
        console.log('LEAVE: ' + speler.naam);
        if (wachtend && wachtend.id === speler.id)
            wachtend = null;
        game.spelerStopt(speler);
    });
});
//# sourceMappingURL=server.js.map