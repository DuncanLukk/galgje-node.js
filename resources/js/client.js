// Connect to websocket
//noinspection NodeModulesDependencies
var socket = io.connect('http://localhost:8000');

socket.on('connect', function () {
    // Verbinding gemaakt
    var id = '/#' + socket.io.engine.id,
        tegenstander = null;

    console.log('Client ID: ' + id);

    socket.on('GameCreated', function (data) {
        console.log('Game created');

        // Klaar om te beginnen, tegenstander zoeken
        $('.woord').html(data.streepjes);
        $('.pogingen').fadeIn().find('.aantalPogingen').html(data.pogingen);

        socket.on('StartGame', function (data) {
            if (data.speler1 === id) tegenstander = data.speler2;
            else if (data.speler2 === id) tegenstander = data.speler1;

            if (tegenstander) {
                // Tegenstander gevonden
                console.log('Start game');

                $('#zoeken').hide();
                $('#gevonden, #letter').fadeIn();

                socket.on('Respons', function (data) {
                    // Server stuurt respons
                    if (data.id === id) {
                        // Voor speler
                        console.log('Respons');

                        if (!data.gameOver) {
                            $('#woord1').html(data.raadString);
                            $('#pogingen1').find('> span').html(data.pogingen);

                            if (data.alGeraden)
                                $('#al-geraden').fadeIn().html(" (<span id='geradenLetter'>" + data.letter + "</span> al geraden)");
                            else $('#al-geraden').hide();
                        } else {
                            $('#woord1').html(data.raadString);
                            gameOver(data.gewonnen);
                        }
                    } else if (data.id === tegenstander) {
                        // Voor tegenstander
                        if (!data.gameOver) {
                            var raadString = data.raadString,
                                verborenString = '';

                            // Letters verbergen
                            for (var i = 0; i < raadString.length; i++) {
                                var raadChar = raadString[i];
                                verborenString += (raadChar === '_' || raadChar === ' ' ? raadString[i] : '*');
                            }

                            $('#woord2').html(verborenString);
                            $('#pogingen2').find('> span').html(data.pogingen);
                        } else {
                            $('#woord2').html(data.raadString);
                            gameOver(!data.gewonnen);
                        }
                    }
                });

                function gameOver(gewonnen) {
                    $('#letter').hide();
                    $('#restart').fadeIn().click(function () {
                        location.reload();
                    });

                    var $speler = $('#pogingen1'),
                        $tegenstander = $('#pogingen2');

                    if (gewonnen) {
                        $speler.addClass('gewonnen').html('Gewonnen!');
                        $tegenstander.addClass('verloren').html('Verloren');

                        // Gewonnen geluid
                        if ($('div.gewonnen').length) {
                            new Audio('../resources/audio/victory.mp3').play();
                        }
                    } else {
                        $tegenstander.addClass('gewonnen').html('Gewonnen!');
                        $speler.addClass('verloren').html('Verloren');

                        // Verloren geluid
                        if ($('div.verloren').length) {
                            new Audio('../resources/audio/fail.mp3').play();
                        }
                    }
                }

                $(function () {
                    function getLetter() {
                        var input = $.trim($('#letter').val()),
                            letter = '';
                        if (input) letter = input.substr(input.length - 1, 1);
                        return letter;
                    }

                    $('#letter').on('input', function () {
                        // Max 1 letter invoeren
                        var letter = getLetter();
                        if (letter) $(this).val(letter);
                    });

                    $('#verzendKnop').click(function (e) {
                        e.preventDefault();
                        var letter = getLetter();
                        if (letter) socket.emit('RaadLetter', letter);
                        $('#letter').val('');
                    });
                });
            }
        });
    });

});

