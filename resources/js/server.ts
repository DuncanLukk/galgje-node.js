///<reference path="typings/server.d.ts"/>

import Socket = SocketIO.Socket;
var http = require('http'),
    server = http.createServer(handler),
    poort = process.env.PORT | '8000';

// De server start op een bepaalde poort en laat dit weten via de console
server.listen(poort);
console.log("Server ingeschakeld op poort " + poort);

function handler(request, response) {
    response.writeHead(200, {"Content-Type": 'text/plain'});
    response.write("De server staat aan");
    response.end();
    console.log('Server staat aan');
}

//De iolistener wordt aangemaakt
var io = require('socket.io'),
    ioListener = io.listen(server, {});

//Een random woord wordt gekozen uit de array
module Random {
    var woorden:string[] = ['naaste', 'tukker', 'roeidol', 'voornaam', 'scheur', 'seinhuis', 'speeluur', 'horloge',
        'traineren', 'spreng', 'starten', 'sleutel', 'oogstfeest', 'toevoegen', 'cataloog', 'doodstil', 'trailer',
        'vlassig', 'onthand', 'graskaas', 'scheen', 'minuut', 'opereren', 'lekken', 'cacaoboon', 'bloemkelk', 'sjiiet',
        'volume', 'ballon', 'rijpaard', 'seniel', 'galappel', 'meevallen', 'meerkoet', 'torsen', 'engagement', 'avance',
        'zuivel', 'feesten', 'digressie', 'rijten', 'reviseren', 'meeprater', 'tumult', 'kopwerk', 'keramiek',
        'traiteur', 'appelflap', 'oorlam', 'deciel', 'overhoren', 'mesten', 'chintz', 'palmboom', 'trapper',
        'aanhaling', 'pagaaien', 'broedei', 'bagatel', 'sloerie', 'indiaans', 'trefzeker', 'teisteren', 'pronkboon',
        'doofpot', 'gravel', 'roomboter', 'pantry', 'klaslokaal', 'onoorbaar', 'vuurbol', 'vangnet', 'expireren',
        'offshore', 'bereden', 'afloop', 'register', 'hendel', 'nylons', 'trilling', 'enerveren', 'radius', 'adhesie',
        'rammen', 'clerus', 'wassen', 'bastaard', 'loeven', 'bakkie', 'glanzen', 'bijtijds', 'kronkelen', 'meneer',
        'penant', 'tonrond', 'luiaard', 'verdoofd', 'rabbijn', 'zaadlob', 'bolletje'];

    export function getWoord():string {
        return woorden[Math.floor(Math.random() * woorden.length)];
    }
}

module GameModule {
    // --- Private ---
    var games:Game[] = [];

    class Game {
        public host:Speler;
        public tegenstander:Speler;
        public controller:GameController;
        public woord:string;

        private _gameOver:boolean = false;
        private _winnaar:Speler = null;

        constructor(host:Speler) {
            this.host = host;
            this.controller = new GameController(this);
            this.woord = Random.getWoord();
        }

        public gameOver(winnaar:Speler):void {
            this._gameOver = true;
            this._winnaar = winnaar;
        }

        public getGameOver():boolean {
            return this._gameOver;
        }

        public heeftGewonnen(speler:Speler):boolean {
            return this._winnaar && this._winnaar.id === speler.id;
        }
    }

    // --- Public ---
    export class GameController {
        private game:Game;

        constructor(game:Game) {
            this.game = game;
        }

        //Spelers worden aan elkaar gekoppeld als tegenstanders
        public setTegenstander(tegenstander:Speler) {
            var game = this.game;
            game.tegenstander = tegenstander;
            console.log(game.host.naam + " en " + game.tegenstander.naam + ' zijn nu tegenstanders');
        }

        public startGame() {
            this.emit('StartGame', {
                speler1: this.game.host.id,
                speler2: this.game.tegenstander.id
            });
        }

        public emit(event:string, data):void {
            this.game.host.socket.emit(event, data);
            this.game.tegenstander.socket.emit(event, data);
        }

        public getRaadString(geradenLetters:string[]):string {
            var woord = this.game.woord,
                raadString = '';

            for (var i = 0; i < woord.length; i++)
                raadString += (geradenLetters.indexOf(woord[i]) >= 0 ? woord[i] : '_') + ' ';

            return raadString;
        }

        public getGameOver():boolean {
            return this.game.getGameOver();
        }

        public heeftGewonnen(speler:Speler):boolean {
            return this.game.heeftGewonnen(speler);
        }

        public raadLetter(speler:Speler, letter:string):void {
            var game = this.game;
            if (game.woord.indexOf(letter) === -1) {
                if (speler.pogingen <= 1) game.gameOver(speler);
                else speler.pogingen--;
            }
        }

        public gameOver(winnaar:Speler):void {
            this.game.gameOver(winnaar);
        }

        public spelerStopt(speler:Speler):void {
            var game = this.game;
            this.gameOver(speler.id === game.host.id ? game.tegenstander : game.host);
        }
    }

    export class Speler {
        public id:string;
        public naam:string;
        public socket:Socket;
        public pogingen:number = 10;

        constructor(socket:Socket, naam:string) {
            this.id = socket.id;
            this.naam = naam;
            this.socket = socket;
        }
    }

    //Een nieuwe game wordt aangemaakt
    export function newGame(speler:Speler):GameController {
        var game = new Game(speler);
        games.push(game);
        return game.controller;
    }

    //Waneer een game beschikbaar is, zal een speler die game joinen
    export function joinGame(host:Speler, tegenstander:Speler):GameController {
        var gameCtrl:GameController = null;
        for (var i = 0; i < games.length; i++)
            if (games[i].host === host) gameCtrl = games[i].controller;

        gameCtrl.setTegenstander(tegenstander);
        return gameCtrl;
    }
}


import randomWoord = Random.getWoord;
import GameController = GameModule.GameController;
import Speler = GameModule.Speler;

var wachtend:Speler = null; // Wachtende speler
var spelernr = 1;

ioListener.on('connection', function (socket:Socket) {
    var speler:Speler = new Speler(socket, 'Speler ' + spelernr++);
    console.log('JOIN: ' + speler.naam);

    var game:GameController = null,
        geradenLetters = [],
        kanStarten = false;

    // Tegenstander vinden of wachten
    if (wachtend !== null) {
        console.log('Er wacht al iemand op ' + speler.naam);
        game = GameModule.joinGame(wachtend, speler);
        kanStarten = true;
        wachtend = null;
    } else {
        console.log(speler.naam + ' moet wachten');
        game = GameModule.newGame(speler);
        wachtend = speler;
    }

    // Begin game
    socket.emit('GameCreated', {
        streepjes: game.getRaadString([]),
        pogingen: speler.pogingen
    });

    if (kanStarten) game.startGame();

    // Speler raadt
    socket.on('RaadLetter', function (letter) {
        var alGeraden = false;

        if (geradenLetters.indexOf(letter) >= 0) {
            // Letter is al geraden
            alGeraden = true;
        } else {
            geradenLetters.push(letter);
            game.raadLetter(speler, letter);
        }

        var raadString = game.getRaadString(geradenLetters);
        if (raadString.indexOf('_') < 0) {
            // Alles geraden
            game.gameOver(speler);
        }

        game.emit('Respons', {
            id: speler.id,
            letter: letter,
            alGeraden: alGeraden,
            raadString: raadString,
            pogingen: speler.pogingen,
            gameOver: game.getGameOver(),
            gewonnen: game.heeftGewonnen(speler)
        });
    });

    socket.on('disconnect', function () {
        // TODO: Disconnect
        console.log('LEAVE: ' + speler.naam);

        if (wachtend && wachtend.id === speler.id) wachtend = null;
        game.spelerStopt(speler);
    });
});